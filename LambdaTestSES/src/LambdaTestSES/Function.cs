using Amazon;
using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace LambdaTestSES;

public class Function
{
    private EmailSettings emailSettings;

    public async Task FunctionHandler(S3Event inputEvent, ILambdaContext context)
    {
        var s3Event = inputEvent.Records?[0].S3;
        
        using (var s3Client = new AmazonS3Client())
        {
            var s3Request = new GetObjectRequest
            {
                BucketName = s3Event.Bucket.Name,
                Key = s3Event.Object.Key
            };
            try
            {
                Console.WriteLine("Requesting object from S3");
                var response = await s3Client.GetObjectAsync(s3Request);
                Console.WriteLine("Object received");
                using (StreamReader reader = new StreamReader(response.ResponseStream))
                {
                    var contents = reader.ReadToEnd();
                    emailSettings = JsonConvert.DeserializeObject<EmailSettings>(contents);
                    Console.WriteLine("JSON parsed successfully");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("The object was not received");
                Console.WriteLine("Error message: " + ex.Message);
        
            }
        }
        
        using (var client = new AmazonSimpleEmailServiceClient(RegionEndpoint.EUWest1))
        {
            var sendRequest = new SendEmailRequest
            {
                Source = emailSettings.senderAddress,
                Destination = new Destination
                {
                    ToAddresses =
                        new List<string> { emailSettings.receiverAddress }
                },
                Message = new Message
                {
                    Subject = new Content("Amazon SES test email"),
                    Body = new Body
                    {
                        Text = new Content
                        {
                            Charset = "UTF-8",
                            Data = "This email was sent through Amazon SES using the AWS SDK for .NET."
                        }
                    }
                }
            };
            
            try
            {
                Console.WriteLine("Sending email using Amazon SES...");
                var response = await client.SendEmailAsync(sendRequest);
                Console.WriteLine("The email was sent successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("The email was not sent.");
                Console.WriteLine("Error message: " + ex.Message);
        
            }
        }
    }
}
