namespace LambdaTestSES;

public class EmailSettings
{
    public string senderAddress { get; set; }
    public string receiverAddress { get; set; }
}